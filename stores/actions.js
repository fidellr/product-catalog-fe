import { fetchProductListApi } from '../utils/api-service/products';
import * as types from './actionTypes';

export const fetchProductList = () => (dispatch) => {
  dispatch({
    type: types.FETCH_PRODUCT_LIST,
  });

  fetchProductListApi()
    .then(({ data }) => {
      dispatch({
        type: types.FETCH_PRODUCT_LIST_SUCCESS,
        payload: {
          productList: data,
        },
      });
    })
    .catch((error) => {
      dispatch({
        type: types.FETCH_PRODUCT_LIST_FAILED,
        payload: {
          error,
        },
      });
    });
};

export const resetProductList = () => (dispatch) => {
  dispatch({
    type: types.RESET_PRODUCT_LIST,
  });
};
