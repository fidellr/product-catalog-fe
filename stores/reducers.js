//#region MODULE IMPORTS
import * as types from './actionTypes';
//#endregion

const initialState = {
  productList: null,
  productListError: null,
  isProductListLoading: false,
};

const productCatalogReducer = (state = initialState, { type, payload }) => {
  const actions = {
    [types.FETCH_PRODUCT_LIST]: () => {
      return {
        ...state,
        isProductListLoading: true,
      };
    },
    [types.FETCH_PRODUCT_LIST_SUCCESS]: () => {
      return {
        ...state,
        isProductListLoading: false,
        productList: payload.productList,
      };
    },
    [types.FETCH_PRODUCT_LIST_FAILED]: () => {
      return {
        ...state,
        isProductListLoading: false,
        productListError: payload.error,
      };
    },
    [types.RESET_PRODUCT_LIST]: () => {
      return {
        ...state,
        productList: null,
        isProductListLoading: false,
        productListError: null,
      };
    },
    DEFAULT: () => state,
  };

  return (actions[type] || actions.DEFAULT)();
};

export default productCatalogReducer;
