//#region PACKAGE IMPORTS
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SwiperCore, { Thumbs } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import ErrorNotify from '../../components/ErrorNotify';
//#endregion

//#region MODULE IMPORTS
import Paper from '../../components/Paper';
import { fetchProductDetail, hydrateProductDetail } from '../../stores/actions';
//#endregion

//#region STYLESHEET IMPORTS
import styles from '../../styles/pages/ProductDetail/ProductDetail.module.scss';
import { getProductDetailApi } from '../../utils/api-service/products';
import { formatCurrency } from '../../utils/helper/number';
//#endregion

SwiperCore.use([Thumbs]);

const ProductDetail = ({ data, error }) => {
  //#region STATE
  const productDetail = data;
  const [images, setGatherImages] = useState(null);
  const [thumbnailSwiper, setThumbnailSwiper] = useState(null);
  const [topSwiper, setTopSwiper] = useState(null);
  const [currentTopImage, setCurrentTopImage] = useState(0);
  const [pickedVariance, setPickVariance] = useState(null);
  const isError = error !== null;
  //#endregion

  //#region LIFECYCLE
  useEffect(() => {
    if (productDetail) {
      const variances = productDetail.variance.map((item) => ({
        image_url: item.image_url,
        color: item.color,
      }));
      setGatherImages([
        { image_url: productDetail.image_url, color: productDetail.color },
        ...variances,
      ]);
    }
  }, [productDetail]);
  //#endregion

  //#region HANDLER
  const handleChangeTopImage = (itemIndex) => {
    setCurrentTopImage(itemIndex);
  };
  const handlePickVariant = (itemIndex) => {
    setPickVariance(itemIndex);
    setCurrentTopImage(itemIndex);
    topSwiper.slideTo(itemIndex, false, false);
  };
  //#endregion

  //#region RENDERER
  const renderProductImages = () => {
    if (!images) return null;

    return (
      <div className={styles.productImagesContainer}>
        <Swiper
          className={`${styles.swiper_container} ${styles.gallery_top}`}
          spaceBetween={10}
          onSwiper={setTopSwiper}
          onSlideChange={(swiper) => handleChangeTopImage(swiper.activeIndex)}
          thumbs={{ swiper: thumbnailSwiper }}
          lazy
        >
          {images.map((item) => (
            <SwiperSlide
              key={item.color}
              className={styles['swiper-slide']}
              style={{ backgroundImage: `url('${item.image_url}')` }}
            />
          ))}
        </Swiper>
        <Swiper
          className={`${styles.swiper_container} ${styles.gallery_thumbs}`}
          spaceBetween={5}
          slidesPerView={4}
          onSwiper={setThumbnailSwiper}
          onClick={(swiper) => setCurrentTopImage(swiper.clickedIndex)}
          watchSlidesVisibility
          watchSlidesProgress
          lazy
        >
          {images.map((item, i) => (
            <SwiperSlide
              key={item.color}
              className={`${styles['swiper-slide']} ${
                currentTopImage !== i ? '' : styles['swiper-slide-thumb-active']
              }`}
              style={{ backgroundImage: `url('${item.image_url}')` }}
            />
          ))}
        </Swiper>
      </div>
    );
  };

  const renderProductInfo = () => {
    return (
      <div className={styles.productInfoContainer}>
        <div className={styles.productNameContainer}>
          <h3 className={styles.productName}>{productDetail.product_name}</h3>
          <span>Terjual {productDetail.sold} Produk</span>
        </div>
        <div
          className={`${styles.productSectionContainer} ${styles.productPriceContainer}`}
        >
          <p className={styles.productSectionTitle}>Harga</p>
          <p className={styles.productPrice}>
            Rp. {formatCurrency(productDetail.price)}
          </p>
        </div>
        <div
          className={`${styles.productSectionContainer} ${styles.productVariantContainer}`}
        >
          <p className={styles.productSectionTitle}>Pilih Variant</p>
          <div className={styles.variantItemsContainer}>
            {images &&
              images.map((item, i) => (
                <div
                  key={item.color}
                  className={`${styles.variantItem} ${
                    pickedVariance === i ? styles.pickedVariant : ''
                  }`}
                  onClick={() => handlePickVariant(i)}
                >
                  <p>{item.color}</p>
                </div>
              ))}
          </div>
        </div>
        <div
          className={`${styles.productSectionContainer} ${styles.productStockContainer}`}
        >
          <p className={styles.productSectionTitle}>Stok Tersisa</p>
          <p className={styles.productStock}>
            {productDetail.stock - productDetail.sold}
          </p>
          &nbsp;Buah
        </div>
      </div>
    );
  };

  const renderPageContent = () => {
    if (isError) {
      return <ErrorNotify />;
    }

    return (
      <>
        {renderProductImages()}
        {renderProductInfo()}
      </>
    );
  };
  //#endregion

  return (
    <Paper isRounded className={styles.productDetailPaper}>
      {renderPageContent()}
    </Paper>
  );
};

export async function getServerSideProps(ctx) {
  const id = ctx.params.id;
  const props = {
    data: null,
    error: null,
  };
  try {
    const { data } = await getProductDetailApi(id);
    return {
      props: {
        ...props,
        data,
      },
    };
  } catch (error) {
    return {
      props: {
        ...props,
        error,
      },
    };
  }
}

export default ProductDetail;
