//#region PACKAGE IMPORTS
import { useRouter } from 'next/router';
import { Fragment, useEffect } from 'react';
import { FiShoppingBag } from 'react-icons/fi';
import { useDispatch, useSelector } from 'react-redux';
import ErrorNotify from '../components/ErrorNotify';
//#endregion

//#region MODULE IMPORTS
import Paper from '../components/Paper';
import ProductCard from '../components/ProductCard';
import Shimmer from '../components/Shimmer';
import { fetchProductList, resetProductList } from '../stores/actions';
//#endregion

//#region STYLESHEET IMPORTS
import styles from '../styles/pages/ProductList/ProductList.module.scss';
//#endregion

export default function Home() {
  //#region ROUTES
  const router = useRouter();
  //#endregion

  //#region STATE
  const state = useSelector((state) => state);
  const dispatch = useDispatch();
  const isLoading = !state.productList || state.isProductListLoading;
  const isError = state.productListError !== null;
  //#endregion

  //#region LIFECYCLE
  useEffect(() => {
    dispatch(fetchProductList());

    return () => {
      dispatch(resetProductList());
    };
  }, [dispatch]);
  //#endregion

  //#region HANDLER
  const handleNavigateToProductDetail = (id) => {
    router.push(`/product/${id}`);
  };
  //#endregion

  //#region RENDERER
  const renderProductPaperTitle = () => {
    return (
      <div className={styles.paperTitleContainer}>
        <div className={styles.productShownIcon}>
          <div className={styles.iconWrapper}>
            <FiShoppingBag stroke="#ffc352" />
          </div>
        </div>

        {isLoading ? (
          <Shimmer className={styles.titleShimmer} />
        ) : (
          <p>
            Menampilkan <b>{state.productList.length} produk</b> spesial untuk Anda
          </p>
        )}
      </div>
    );
  };

  const renderProductItems = () => {
    if (isLoading) {
      return (
        <>
          {Array.from({ length: 8 }).map((_, i) => {
            return (
              <ProductCard key={i} className={styles.productItem} isLoading />
            );
          })}
        </>
      );
    }

    return state.productList.map((product) => (
      <ProductCard
        key={product.id}
        isImageLazyload
        onClick={() => handleNavigateToProductDetail(product.id)}
        className={styles.productItem}
        imageUrl={product.image_url}
        name={product.product_name}
        price={product.price}
        sold={product.sold}
        stock={product.stock}
      />
    ));
  };

  const renderPageContent = () => {
    if (isError) {
      return <ErrorNotify />;
    }

    return (
      <>
        {renderProductPaperTitle()}
        <div className={styles.productItemsContainer}>
          {renderProductItems()}
        </div>
      </>
    );
  };
  //#endregion

  return (
    <Fragment>
      <Paper className={styles.productListPaper} isRounded>
        {renderPageContent()}
      </Paper>
    </Fragment>
  );
}
