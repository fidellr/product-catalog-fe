//#region PACKAGE IMPORTS
import Head from "next/head";
import { Provider } from 'react-redux';
import NProgress from 'nextjs-progressbar';
//#endregion

//#region MODULE IMPORTS
import Layout from '../components/Layout';
import { useStore } from '../stores';
//#endregion

//#region STYLESHEET IMPORTS
import '../styles/globals.scss';
import 'swiper/swiper.scss';
//#endregion

function MyApp({ Component, pageProps }) {
  const store = useStore(pageProps.initialReduxState);
  return (
    <Provider store={store}>
      <Layout>
      <Head>
        <title>Catalogue.</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
        <NProgress
          color="#ffc352"
          startPosition={0.3}
          stopDelayMs={200}
          options={{
            showSpinner: false,
          }}
          height="3"
        />
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
