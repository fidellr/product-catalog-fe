To run the development server, run command:

```bash
npm run dev
# or
yarn dev
```

To run the production server, run command :
1.
```bash
npm run build
# or
yarn build
```
2.
```bash
npm run start
# or
yarn start
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.