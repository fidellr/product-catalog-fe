const BASE_URL =
  'https://my-json-server.typicode.com/fidellr/product-catalog-mockapi';
export const PRODUCT_CATALOG_URL = `${BASE_URL}/products`;
