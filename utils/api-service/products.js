import axios from 'axios';
import { PRODUCT_CATALOG_URL } from './constants';

export const fetchProductListApi = () => axios.get(PRODUCT_CATALOG_URL);

export const getProductDetailApi = (id) =>
  axios.get(`${PRODUCT_CATALOG_URL}/${id}`);
