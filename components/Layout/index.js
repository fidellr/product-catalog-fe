//#region PACKAGE IMPORTS
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { FiUser, FiChevronLeft } from 'react-icons/fi';
//#endregion

//#region STYLESHEET IMPORTS
import styles from './Layout.module.scss';
//#endregion

const Layout = ({ children }) => {
  const router = useRouter();
  const [scrollPosition, setScrollPosition] = useState(0);

  const getScrollPosition = () => {
    setScrollPosition(window.scrollY);
  };

  useEffect(() => {
    window.addEventListener('scroll', getScrollPosition);

    return () => {
      window.removeEventListener('scroll', getScrollPosition);
    };
  }, []);

  const renderNavbar = () => {
    const boxShadow =
      scrollPosition >= 50 ? { boxShadow: 'gainsboro 0px 0px 7px 1px' } : null;
    const isProductListPage = router.pathname === '/';

    return (
      <div className={styles.navbar} style={boxShadow}>
        <div className={styles.navbarContent}>
          <h3
            className={isProductListPage ? styles.isProductListPage : ''}
            onClick={() => router.push('/')}
          >
            Catalogue.
          </h3>
          {!isProductListPage && (
            <FiChevronLeft onClick={() => router.push('/')} />
          )}

          <div className={styles.navigator}>
            <div className={styles.profilePicture}>
              <FiUser />
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <div className={styles.layoutContainer}>
      {renderNavbar()}
      <main className={styles.main}>{children}</main>
    </div>
  );
};

export default Layout;
