//#region PACKAGE IMPORTS
import { FiFrown } from 'react-icons/fi';
//#endregion

//#region STYLESHEET IMPORTS
import styles from './ErrorNotify.module.scss';
//#endregion

const ErrorNotify = () => {
  return (
    <div className={styles.errorContainer}>
      <FiFrown />
      <div className={styles.errorMessageContainer}>
        <h3 className={styles.errorMessage}>
          Oops...
        </h3>
        Something went wrong, please refresh the page
      </div>
    </div>
  );
};

export default ErrorNotify;
