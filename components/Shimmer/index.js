//#region STYLESHEET IMPORTS
import styles from './Shimmer.module.scss';
//#endregion

const Shimmer = ({ className }) => {
  const classes = !className
    ? styles.shimmer
    : `${styles.shimmer} ${className}`;
  return <div className={classes} />;
};

export default Shimmer;
