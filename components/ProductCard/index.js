// #region PACKAGE IMPORTS
import dynamic from 'next/dynamic';
import LazyLoad from 'react-lazyload';
//#endregion

//#region STYLESHEET IMPORTS
import { formatCurrency } from '../../utils/helper/number';
import ProgressBar from '../ProgressBar';
import Shimmer from '../Shimmer';
import styles from './ProductCard.module.scss';
//#endregion

const ProductCard = ({
  onClick,
  className,
  isLoading,
  imageUrl,
  name,
  price,
  sold,
  stock,
  isImageLazyload,
}) => {
  const renderProductImage = () => {
    if (isLoading || !imageUrl.length) {
      return (
        <Shimmer className={`${styles.productImage} ${styles.imageShimmer}`} />
      );
    }

    if (isImageLazyload) {
      return (
        <LazyLoad height={350} once>
          <img src={imageUrl} className={styles.productImage} alt={name} />
        </LazyLoad>
      );
    }

    return <img src={imageUrl} className={styles.productImage} alt={name} />;
  };

  const renderProductInfo = () => {
    if (isLoading || !imageUrl.length) {
      return (
        <>
          <Shimmer className={styles.productName} />
          <Shimmer className={styles.productPrice} />
        </>
      );
    }

    const soldPercentage = Math.round((sold / stock) * 100);

    return (
      <>
        <p className={styles.productName}>{name}</p>
        <p className={styles.productPrice}>Rp. {formatCurrency(price)}</p>
        <p className={styles.productSold}>Terjual</p>
        <ProgressBar
          className={styles.soldPercentage}
          percentage={soldPercentage}
        />
      </>
    );
  };

  const containerClasses = !className
    ? styles.cardContainer
    : `${styles.cardContainer} ${className}`;
  const infoContainerClasses = !isLoading
    ? styles.productInfoContainer
    : `${styles.productInfoContainer} ${styles.productInfoContainerShimmer}`;

  return (
    <div className={containerClasses} onClick={onClick}>
      {renderProductImage()}
      <div className={infoContainerClasses}>{renderProductInfo()}</div>
    </div>
  );
};

export default ProductCard;
