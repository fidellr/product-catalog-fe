import styles from "./Paper.module.scss";

const Paper = ({ children, className, isRounded, width, height }) => {
  className =
    (isRounded ? styles.paperContainer__rounded : styles.paperContainer) +
    (!className ? "" : " " + className);

  return (
    <div className={className} style={{ width, height }}>
      {children}
    </div>
  );
};

export default Paper;
