//#region STYLESHEET IMPORTS
import styles from './ProgressBar.module.scss';
//#endregion

const ProgressBar = ({ className, percentage }) => {
  const progressPercent = !percentage ? '100%' : `${percentage}%`;
  const barClasses = !className ? styles.bar : `${styles.bar} ${className}`;

  return (
    <div className={barClasses}>
      <div className={styles.meter} style={{ width: progressPercent }}>
        <div className={styles.progress}>
          <p>{percentage}%</p>
        </div>
      </div>
    </div>
  );
};

export default ProgressBar;
